package pl.codementors.JavaFXTransformations;

import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import pl.codementors.JavaFXTransformations.Shapes.Star;

public class TransformationsMain extends Application {
    private static final int sceneSize = 600;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        Star star = new Star(100, sceneSize / 2, sceneSize / 2);
        star.setFill(Color.RED);

        ToggleButton button = new ToggleButton();
        button.setText("Start/Stop");

        button.setUserData(sequential(star));
        button.setOnAction(event -> {
            ToggleButton tb = (ToggleButton) event.getSource();
            Transition transite = (Transition) tb.getUserData();
            if (tb.isSelected()) {
                transite.play();
            } else {
                transite.pause();
            }
        });

        BorderPane rootPane = new BorderPane();
        Pane pCenter = new Pane();
        pCenter.getChildren().add(star);
        pCenter.setMinHeight(600);
        pCenter.setMinWidth(600);

        HBox pLeft = new HBox();
        pLeft.getChildren().add(button);

        rootPane.setCenter(pCenter);
        rootPane.setLeft(pLeft);
        rootPane.setStyle("-fx-background-color: #000000");
        rootPane.setMinSize(sceneSize, sceneSize);

        Scene scene = new Scene(rootPane, sceneSize, sceneSize, Color.BLACK);
        stage.setMinWidth(sceneSize);
        stage.setMinHeight(sceneSize);
        stage.setScene(scene);
        stage.show();
    }

    private Transition rotating(Node node) {
        RotateTransition rt = new RotateTransition();
        rt.setAutoReverse(false);
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setDuration(Duration.millis(2000));
        rt.setNode(node);
        rt.setCycleCount(1);
        return rt;
    }

    private Transition moving(Node node, double toX, double toY) {
        TranslateTransition st = new TranslateTransition();
        st.setAutoReverse(false);
        st.setCycleCount(1);
        st.setDuration(Duration.millis(2000));
        st.setNode(node);
        st.setToX(toX);
        st.setToY(toY);
        return st;
    }

    private Transition parallelRotatingAndMoving(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(rotating(node));
        pt.getChildren().add(moving(node, 300, 0));
        pt.setAutoReverse(false);
        return pt;
    }

    private Transition fade(Node node, double from, double to) {
        FadeTransition ft = new FadeTransition(Duration.millis(2000), node);
        ft.setFromValue(from);
        ft.setToValue(to);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        return ft;
    }

    private Transition parallelFadingAndMovingDown(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(fade(node, 1.0, 0.1));
        pt.getChildren().add(moving(node, 0, 300));
        pt.setAutoReverse(false);
        return pt;
    }

    private Transition parallelFadingAndMovingUp(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(fade(node, 0.1, 1.0));
        pt.getChildren().add(moving(node, -300, 0));
        pt.setAutoReverse(false);
        return pt;
    }

    private Transition fill(Shape node) {
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.BLUE);
        ft.setShape(node);
        ft.setCycleCount(2);
        ft.setAutoReverse(true);
        ft.setDuration(Duration.millis(1000));
        return ft;
    }

    private Transition parallelColorChangingAndMoving(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(fill(node));
        pt.getChildren().add(moving(node, 0, 0));
        pt.setAutoReverse(false);
        return pt;
    }

    private Transition sequential(Shape node) {
        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(
                moving(node, 0, -300),
                parallelRotatingAndMoving(node),
                parallelFadingAndMovingDown(node),
                parallelFadingAndMovingUp(node),
                parallelColorChangingAndMoving(node));
        for (Animation a : st.getChildren()) {
            a.setCycleCount(1);
        }
        st.setCycleCount(Timeline.INDEFINITE);
        return st;
    }

}
