package pl.codementors.JavaFXTransformations.Shapes;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class Star extends Polygon {

    private static final int NUM_OF_POINTS = 11;
    private static final double ALPHA = (2 * Math.PI) / 10;

    private static double[] createPoints(final int radius, final int x, final int y) {
        double[] points = new double[22];
        int pointIndex = 0;
        for (int i = NUM_OF_POINTS; i != 0; i--) {
            int r = radius * (i % 2 + 1) / 2;
            double omega = ALPHA * i;

            points[pointIndex++] = (r * Math.sin(omega)) + x;
            points[pointIndex++] = (r * Math.cos(omega)) + y;
        }
        return points;
    }

    public Star(int radius, int x, int y) {
        super(createPoints(radius, x, y));
    }

    public Star(int i, int i1, int i2, Color blue){
    }
}
